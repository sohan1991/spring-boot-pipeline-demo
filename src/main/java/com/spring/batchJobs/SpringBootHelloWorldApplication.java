package com.spring.batchJobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringBootHelloWorldApplication {

    private static final Logger logger = LoggerFactory.getLogger(SpringBootHelloWorldApplication.class);

	public static void main(String[] args) {
		logger.info("Main Class Called");
		SpringApplication.run(SpringBootHelloWorldApplication.class, args);
		
	}
}