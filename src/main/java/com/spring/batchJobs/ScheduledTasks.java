package com.spring.batchJobs;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.spring.batchJobs.util.UuidGenerator;

import ch.qos.logback.core.db.dialect.MySQLDialect;

@Component
public class ScheduledTasks {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"MM/dd/yyyy HH:mm:ss");
	private static final Logger logger = LoggerFactory.getLogger(SpringBootHelloWorldApplication.class);

	@Scheduled(fixedRate = 10000)// Will be called at every 10 Seconds
	public void performTask() {

		System.out.println("Regular task performed at "
				+ dateFormat.format(new Date()));
		
		logger.info("performTask Called");
		//UuidGenerator objUuidGenerator = new UuidGenerator();
		//String uuidStr = objUuidGenerator
		String uuidStr=UuidGenerator.myMethod();
		logger.info("CSV File Generated Successfully -----"+uuidStr);
	}
	//Initial Delay of 1 second.. After Every 10 Seconds it will be called
	@Scheduled(initialDelay = 1000, fixedRate = 10000)
	public void performDelayedTask() {

		System.out.println("Delayed Regular task performed at "
				+ dateFormat.format(new Date()));
		logger.debug("performDelayedTask Called");

	}
	//At Every 5 Seconds this method will be invoked
	//@Scheduled(cron = "*/5 * * * * *")
    @Scheduled(cron = "${scheduler.configurationLoadRate}")
	public void performTaskUsingCron() {

		System.out.println("Regular task performed using Cron at "
				+ dateFormat.format(new Date()));
		
		//logger.error("performTaskUsingCron Called Error");


	}
}
