package com.spring.batchJobs.util;
import java.security.SecureRandom;
import java.util.UUID;

public class UuidGenerator {

    private static volatile SecureRandom numberGenerator = null;
    private static final long MSB = 0x8000000000000000L;
    
    public static String myMethod() {
    
    System.out.println("Hello World!");
    SecureRandom ng = numberGenerator;
        if (ng == null) {
            numberGenerator = ng = new SecureRandom();
        }

       System.out.println(Long.toHexString(MSB | ng.nextLong()) + Long.toHexString(MSB | ng.nextLong()));
       String uuidString = Long.toHexString(MSB | ng.nextLong()) + Long.toHexString(MSB | ng.nextLong());
	return uuidString;
  }
}
